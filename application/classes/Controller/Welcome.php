<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Welcome extends Controller {

	public function action_index()
	{
		$this->response->body('hello, world!');
	}
	public function action_login()
	{
		if(isset($_GET['username'])){
			$username = $_GET['username'];
		}
		else{
			$username = NULL;
		}
		if(isset($_GET['password'])){
			$password = $_GET['password'];
		}
		else{
			$password = NULL;
		}

		if($username== NULL && $password == NULL){
			echo "Please enter params username and password";
		}
		else{
			$model = new Model_Login;
			$result = $model->get_data($username);
			if($result == NULL){
				echo "email not found";
			}
			elseif($result['password'] == $password){
				echo "login successfull <br/>";
				echo " User's info ->";
				foreach ($result as $key => $value) {
					echo $key." - ".$value;
				}
			}else{
				echo "password not matched";
			}
			// $this->response->body('abc, login!');
		}
	}

	function register(){
		$password = $_GET['password'];
		$email = $_GET['email'];
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			echo "invalid email format";
		}
		if(strlen($password) < 6){
		    echo "invalid password length";
		}
		else{
			$model = new Model_Login;
			$result = $model->register_data($_GET);
			if($result){
				echo "you are now registered";
			}
			else{
				echo "there's some problem inserting data";
			}
		}
	}

	function update(){
		$updateArray = $_GET;
		$email = $_GET['email'];
		$keys = array();
		$updatedata = array();
		foreach ($updateArray as $key => $value) {
			$keys[] = $key;
			$updatedata[] = $value;
		}
		$model = new Model_Login;
		$result = $model->update_date($updateArray, $email);
		if($result){
			echo "successfully updated";
		}
		else{
			echo "update error";
		}
	}

} // End Welcome
